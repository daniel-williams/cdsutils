#!/bin/bash

export EPICS_CAS_INTF_ADDR_LIST=localhost
export EPICS_CAS_SERVER_PORT=58800
export EPICS_CA_ADDR_LIST=localhost:58800

cd lib
python -m ezca.tests
python -m cdsutils.tests
