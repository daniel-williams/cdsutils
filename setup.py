#!/usr/bin/env python

from distutils.core import setup

execfile('lib/cdsutils/_version.py')

setup(
    name = 'cdsutils',
    version = __version__,
    description = 'aLIGO CDS utilities library and CLI',
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://redoubt.ligo-wa.caltech.edu/svn/cdsutils',
    license = 'GNU GPL v3+',

    package_dir = {'': 'lib'},
    packages = [
        'cdsutils',
        'ezca',
        'ezca.emulators',
        'pydv',
        ],
    scripts = [
        'bin/cdsutils',
        'bin/z',
        ],
    )
