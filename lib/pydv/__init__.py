import os
import logging
import matplotlib.pyplot as plt

from plotting import NDSFigure, NDSAxes
from bufferdict import NDSBufferDict, NDSBuffer

DEFAULT_LOG_LEVEL = 'INFO'

def plot(
        channel_names,
        center_time,
        xmin,
        xmax,
        title=None,
        sharex=True,
        sharey=False,
        no_threshold=True,
        no_wd_state=True,
        save=None,
        show=True,
        ):

    def _addHandler(logger):
        formatter = logging.Formatter('%(asctime)s : %(name)s : %(message)s')
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        handler.set_name('tests')
        logger.addHandler(handler)
        logger.setLevel(os.getenv('PYDV_LOG_LEVEL', DEFAULT_LOG_LEVEL).upper())

    _addHandler(NDSAxes.logger)
    _addHandler(NDSFigure.logger)
    _addHandler(NDSBufferDict.logger)
    _addHandler(NDSBuffer.logger)

    fig = plt.figure(FigureClass=NDSFigure,
                     channel_names=channel_names,
                     center_time=center_time,
                     xmin=xmin,
                     xmax=xmax,
                     title=title,
                     sharex=sharex,
                     sharey=sharey,
                     no_threshold=no_threshold,
                     no_wd_state=no_wd_state,)
    try:
        fig.draw_subplots()
    except NDSAxes.Exception as e:
        raise SystemExit("Failed retrieving data or drawing plot.")
    except NDSFigure.Exception as e:
        raise SystemExit("Failed retrieving data or drawing plot.")

    fig.set_size_inches(16*1.5, 9*1.5, forward=True)

    if save:
        plt.savefig(save)
    if show:
        plt.show()

    return fig
