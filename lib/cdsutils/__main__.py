#!/usr/bin/env python

import os
import sys
import imp
import signal

from __init__ import __version__, CMDS

sys.path.insert(0, os.path.dirname(__file__))

PROG = 'cdsutils'

def usage():
    print '''usage: %s <cmd> <args>

Advanced LIGO Control Room Utilites

Available commands:
''' % (PROG)
    for cmd in CMDS:
        f, pathname, description = imp.find_module(cmd)
        try:
            summary = imp.load_module('cdsutils', f, pathname, description).summary
        except AttributeError:
            summary = ''
        except ImportError as e:
            summary = 'NOT SUPPORTED: ' + str(e)
        f.close()
        print '  {0:<12s} {1}'.format(cmd, summary)
    print
    print '  {0:<12s} {1}'.format('version', 'print version info and exit')
    print '  {0:<12s} {1}'.format('help', 'this help')
    print '''
Add '-h' after individual commands for command help.'''

if len(sys.argv) == 1:
    usage()
    sys.exit()

if sys.argv[1] in ['help','-h','--help']:
    usage()
    sys.exit()

if sys.argv[1] in ['version','-v','--version']:
    print 'cdsutils', __version__
    sys.exit()

if sys.argv[1] not in CMDS:
    print >>sys.stderr, "Unknown command: %s" % sys.argv[1]
    print >>sys.stderr, "Type '%s help' for more info." % PROG
    sys.exit(1)

cmd = sys.argv[1]

# handle Ctrl-C
signal.signal(signal.SIGINT, signal.SIG_DFL)

# module import execution for commands
sys.argv = sys.argv[1:]
f, pathname, description = imp.find_module(cmd)
try:
    imp.load_module('__main__', f, pathname, description)
except KeyboardInterrupt:
    raise SystemExit()
finally:
    if f:
        f.close()
