from ezca import Ezca, EzcaError
from _util import split_channel_ifo

summary = "read EPICS channel value"

if __name__ == '__main__':

    # http://docs.python.org/2/library/argparse.html
    import argparse
    parser = argparse.ArgumentParser(description=summary,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('channel',
                        help='channel name',
                        type=str,
                        nargs='+'
                        )
    parser.add_argument('-S',
                        help='retrieve value as string',
                        dest="as_string",
                        action="store_true",
                        )
    args = parser.parse_args()

    if isinstance(args.channel,str):
        channels = [args.channel]
    else:
        channels = args.channel

    ezca = Ezca()
    for channel in channels:
        try:
            __, channel = split_channel_ifo(channel)
            print ezca.read(channel, as_string=args.as_string)
        except EzcaError as e:
            raise SystemExit(e)
