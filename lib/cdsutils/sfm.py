import sys
from ezca import Ezca, SFMask, LIGOFilterError
from ezca.const import BUTTONS_ORDERED, SWSTAT_BITS

summary = "decode/encode filter module switch values"

def usage():
    print """usage: sfm [<cmd>] <args>

%s

commands:
  decode SW1 SW2                  decode SM1 and SM2 values into engaged button names
  decode SWSTAT                   decode SWSTAT value into engaged button names
                                  (NOTE: does not include DECIMATION)
  encode [+e] BUTTON [BUTTON...]  encode list of engaged buttons into switch values
                                  with +e don't include button engaged bits

If the commands names are left out and the encoding/decoding will be guessed.
Filter banks names may be specified by index when encoding
(e.g. '3' instead of 'FM3').""" % summary

if __name__ == '__main__':

    if len(sys.argv) < 2:
        usage()
        sys.exit()

    for arg in sys.argv[1:]:
        if arg in ['help','-h','--help']:
            usage()
            sys.exit()

    cmd = sys.argv[1]
    if cmd == 'decode':
        try:
            if len(sys.argv) == 3:
                SWSTAT = int(sys.argv[2])
                cmd = 'decode_swstat'
            elif len(sys.argv) == 4:
                SW1 = int(sys.argv[2])
                SW2 = int(sys.argv[3])
                cmd = 'decode_sw'
            else:
                sys.exit("Improper number of arguments.")
        except ValueError:
            sys.exit("SW values must be integers")

    elif cmd == 'encode':
        i = 2
        ENGAGED = True
        if sys.argv[2] == '+e':
            ENGAGED = False
            i = 3
        BUTTONS = [b.upper() for b in sys.argv[i:]]
        cmd = 'encode'

    else:
        try:
            if len(sys.argv) == 2:
                SWSTAT = int(sys.argv[1])
                cmd = 'decode_swstat'
            else:
                SW1 = int(sys.argv[1])
                SW2 = int(sys.argv[2])
                cmd = 'decode_sw'
        except ValueError:
            ENGAGED = False
            BUTTONS = [b.upper() for b in sys.argv[1:]]
            cmd = 'encode'


    # decode SWSTAT
    if cmd == 'decode_swstat':
        mask = SFMask.from_swstat(SWSTAT)
        for button in BUTTONS_ORDERED:
            if button in mask:
                print button

    # decode SW1/2
    elif cmd == 'decode_sw':
        buttons = SFMask.from_sw(SW1, SW2).buttons
        for button in BUTTONS_ORDERED:
            if button in buttons:
                print button

    # encode buttons
    elif cmd == 'encode':
        try:
            mask = SFMask.for_buttons_engaged(*BUTTONS, engaged=ENGAGED)
        except LIGOFilterError as e:
            sys.exit("LIGOFilter error: "+str(e))

        print 'SW1: %d' % (mask.SW1)
        print 'SW2: %d' % (mask.SW2)
        print 'SWSTAT: %d' % (mask.SWSTAT)

    else:
        usage()
        sys.exit(1)
