#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Water - channel
# Author: ccw
# Description: Live waterfall plotter for LIGO data
# Generated: Wed Feb  5 15:45:54 2014
##################################################

summary = 'live waterfall plot of NDS channel'
from PyQt4 import Qt
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import sip
import sys, os, inspect
import gnuradio_blocks

class water(gr.top_block, Qt.QWidget):
    def __init__(self, channel="None", samp_rate=None, res=1, bw=None):
        gr.top_block.__init__(self, "Water - " + channel)
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Water - " + channel)
        try:
             self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
             pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "water")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.channel = channel
        self.samp_rate = samp_rate
        self.res = res
        self.bw = bw

        ##################################################
        # Blocks
        ##################################################
        self.qtgui_sink_x_0 = qtgui.sink_f(
        	int(samp_rate/res), #fftsize
        	firdes.WIN_HANN, #wintype
        	0, #fc
        	bw, #bw
        	"QT GUI Plot", #name
        	False, #plotfreq
        	True, #plotwaterfall
        	False, #plottime
        	False, #plotconst
        )
        self.qtgui_sink_x_0.set_update_time(1.0/res)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_layout.addWidget(self._qtgui_sink_x_0_win)
        
        
        self.ligocds_daq_source_f_0 = gnuradio_blocks.daq_source_f(channel)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.ligocds_daq_source_f_0, 0), (self.qtgui_sink_x_0, 0))


# QT sink close method reimplementation
    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "water")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_channel(self):
        return self.channel

    def set_channel(self, channel):
        self.channel = channel
        self.ligocds_daq_source_f_0.set_channel(self.channel)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_res(self):
        return self.res

    def set_res(self, res):
        self.res = res

    def get_bw(self):
        return self.bw

    def set_bw(self, bw):
        self.bw = bw
        self.qtgui_sink_x_0.set_frequency_range(0, self.bw)

if __name__ == '__main__':
    import ctypes
    import os
    if os.name == 'posix':
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"
    parser = OptionParser(option_class=eng_option, usage="%prog: [options] channel")
    #parser.add_option("-c", "--channel", dest="channel", type="string", default="None",
    #    help="Set channel [default=%default]")
    parser.add_option("-r", "--res", dest="res", type="eng_float", default=eng_notation.num_to_str(1),
        help="Set res [default=%default]")
    parser.add_option("-b", "--bw", dest="bw", type="eng_float", default=None,
        help="Set bw [default=%default]")
    (options, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("wrong number of arguments")
    options.channel = args[0]
    #if gr.enable_realtime_scheduling() != gr.RT_OK:
    #    print "Error: failed to enable realtime scheduling."
    qapp = Qt.QApplication(sys.argv)
    
    samp_rate = gnuradio_blocks.get_samp_rate(options.channel)
    if options.bw is None:
        options.bw = samp_rate
    tb = water(channel=options.channel, samp_rate=samp_rate, res=options.res, bw=options.bw)
    tb.start()
    tb.show()
    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()
    tb = None #to clean up Qt widgets

