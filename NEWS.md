Release 1.0.0
=============

First release with the new versioning scheme.

* gpstime module has been split into a separate upstream project
