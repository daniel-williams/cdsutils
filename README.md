cdsutils
========

Various python libraries and command line utilities for interacting
with the CDS front-end system:

  * cdsutils: useful CDS interaction funtions
  * ezca: CDS EPICS interface, used by Guardian
  * pydv: python NDS time series viewer (dataviewer replacement)
  * gpstime: GPS-aware datetime module
